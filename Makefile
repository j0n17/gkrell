##
## Makefile for Make in /home/maire_j/.Cfiles
## 
## Made by 
## Login   <maire_j@epitech.net>
## 
## Started on  Thu Dec 12 21:06:49 2013 
## Last update Sat Jan 25 11:24:06 2014 
##

NAME		= my_GKrell

CC		= g++

RM		= rm -rf

SRC_DIR		= src

OBJ_DIR		= obj

INC_DIR		= inc

SRC		= $(SRC_DIR)/my_gkrell.cpp \
		  $(SRC_DIR)/my_graph.cpp \
		  $(SRC_DIR)/my_text.cpp

OBJ		= $(SRC:.cpp=.o)

CPPFLAGS	= -I$(INC_DIR) \
		  -Wall -Werror -Wextra

all:		$(NAME)

$(NAME):	$(OBJ)
		$(CC) -o $(NAME) $(OBJ) -lcurses

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all
