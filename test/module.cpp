//
// module.cpp for module in /home/maire_j/rendu/piscine_cpp_rush3/test
// 
// Made by 
// Login   <maire_j@epitech.net>
// 
// Started on  Sat Jan 25 13:10:01 2014 
// Last update Sat Jan 25 17:51:53 2014 
//

#include <ctime>
#include <cstdlib>
#include <algorithm>
#include "module.hpp"

IMonitorModule::~IMonitorModule()
{}

IMonitorDisplay::~IMonitorDisplay()
{}

Module::Module(std::string file, bool disp, unsigned int pos)
{
  _file = file;
  _display = disp;
  _pos = pos;
  this->setData();
}

Module::~Module()
{}

void	Module::setData()
{
  std::ifstream	file (_file.c_str());
  std::string	data;
  std::string	content;

  if (file)
    {
      while (std::getline(file, content))
	{
	  data += content;
	  data += '\n';
	}
      file.close();
      _data = data;
    }
}

void		Module::toggleDisplay()
{
  if (_display == true)
    _display = false;
  else
    _display = true;
}

Hostname::Hostname(std::string file, bool display, unsigned int pos) : Module::Module(file, display, pos)
{}


Hostname::~Hostname()
{}

std::string	Hostname::getData()
{
  _data.erase(_data.length() - 1);
  return (_data);
}

Username::Username(std::string file, bool display, unsigned int pos) : Module::Module(file, display, pos)
{}


Username::~Username()
{}

std::string	Username::getData()
{
  _data = getenv("USER");
  return (_data);
}

Version::Version(std::string file, bool display, unsigned int pos) : Module::Module(file, display, pos)
{}


Version::~Version()
{}

std::string     Version::getData()
{
  _data.erase(_data.length() - 1);
  return (_data);
}

Date::Date(std::string file, bool display, unsigned int pos) : Module::Module(file, display, pos)
{}


Date::~Date()
{}

std::string     Date::getData()
{
  time_t	now = time(0);
  char*		dt = ctime(&now);
  _data = dt;
  _data.erase(_data.length() - 1);
  return (_data);
}


Mem::Mem(std::string file, bool display, unsigned int pos) : Module::Module(file, display, pos)
{}


Mem::~Mem()
{}

std::string     Mem::getData()
{
  std::string	memFree;
  std::string	memTotal;

  memFree = _data.substr(_data.find("MemFree:") + 8, _data.find("kB\n") - 9);
  memFree = memFree.substr(memFree.find_last_of(" ") + 1);
  memTotal = _data.substr(_data.find("MemTotal:") + 9, _data.find("kB\n") - 10);
  memTotal = memTotal.substr(memTotal.find_last_of(" ") + 1);
  memTotal += '\n';
  memTotal += memFree;
  return (memTotal);
}

CPU::CPU(std::string file, bool display, unsigned int pos) : Module::Module(file, display, pos)
{}


CPU::~CPU()
{}

std::string     CPU::getData()
{
  std::string	nv;
  std::string	model;
  int		nb;

  nv = _data.substr(_data.rfind("processor") + 12, 2);
  nv = nv.substr(0, 1);
  nb = atoi(nv.c_str());
  nb++;
  nv = (char) nb + 48;
  nv += '\n';
  model = _data.substr(_data.rfind("model name"));
  model = model.substr(13, model.find("\n") - 13);
  nv += model;
  nv += '\n';
  model = model.substr(model.find("@ ") + 2, model.find("GHz"));
  model = model.substr(0, model.length() - 3);
  nv += model;
  return (nv);
}

