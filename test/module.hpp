//
// module.hpp for module in /home/maire_j/rendu/piscine_cpp_rush3/test
// 
// Made by 
// Login   <maire_j@epitech.net>
// 
// Started on  Sat Jan 25 12:19:13 2014 
// Last update Sat Jan 25 16:55:31 2014 
//

#ifndef MODULE_HPP_
# define MODULE_HPP_

#include <fstream>
#include <iostream>
#include <string>

class IMonitorModule
{
public:
  virtual ~IMonitorModule();
  virtual std::string	getData() = 0;
};

class IMonitorDisplay : public IMonitorModule
{
public:
  virtual ~IMonitorDisplay();
  virtual void	toggleDisplay() = 0;
};

class Module : public IMonitorDisplay
{
protected:
  std::string		_file;
  bool			_display;
  unsigned int		_pos;
  std::string		_data;
public:
  Module(std::string, bool, unsigned int);
  virtual ~Module();
  void		setData();
  void		toggleDisplay();
};

class Hostname : public Module
{
public:
  Hostname(std::string, bool, unsigned int);
  ~Hostname();
  std::string	getData();
};

class Username : public Module
{
public:
  Username(std::string, bool, unsigned int);
  ~Username();
  std::string   getData();
};

class Version : public Module
{
public:
  Version(std::string, bool, unsigned int);
  ~Version();
  std::string   getData();
};

class Date : public Module
{
public:
  Date(std::string, bool, unsigned int);
  ~Date();
  std::string   getData();
};

class Mem : public Module
{
public:
  Mem(std::string, bool, unsigned int);
  ~Mem();
  std::string   getData();
};

class CPU : public Module
{
public:
  CPU(std::string, bool, unsigned int);
  ~CPU();
  std::string   getData();
};


#endif /* MODUEL_HPP_ */
