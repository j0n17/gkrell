#include "module.hpp"

int	main(void)
{
  Hostname	mod("/etc/hostname", true, 0);
  Username	mod1("", true, 0);
  Version	mod2("/proc/version", true, 0);
  Date		mod3("", true, 0);
  Mem		mod4("/proc/meminfo", true, 0);
  CPU		mod5("/proc/cpuinfo", true, 0);
  std::cout << mod5.getData() << std::endl;
  std::cout << mod4.getData() << std::endl;
  std::cout << mod.getData() << std::endl;
  std::cout << mod1.getData() << std::endl;
  std::cout << mod2.getData() << std::endl;
  std::cout << mod3.getData() << std::endl;
  return (0);
}
