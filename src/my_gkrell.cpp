/*
** main.c for Project in /home/maire_j/.Cfiles/src
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Dec 12 21:13:04 2013 
// Last update Sat Jan 25 11:34:04 2014 
*/

#include "header.hpp"

void	startGkrell(std::string mode)
{
  if (mode == "text")
    textGkrell();
  else if (mode == "graph")
    graphGkrell();
  else
    std::cout << "Usage : ./my_Gkrell [text] or [graph]" << std::endl;
}

int	main(int ac, char **av)
{
  std::string	mode;

  if (ac == 1)
    mode = "text";
  else if (ac == 2)
    mode.assign(av[1], strlen(av[1]));
  else
    {
      std::cout << "Usage : " << av[0] << " [text] or [graph]" << std::endl;
      return (0);
    }
  startGkrell(mode);
  return (0);
}
